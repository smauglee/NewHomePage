"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Const = (function () {
    function Const() {
    }
    return Const;
}());
//Const.BASE_ENDPOINT = 'http://fitserver.azurewebsites.net/api/';
Const.BASE_ENDPOINT = 'http://localhost:53009/api/';
Const.CONTENT_TYPE = 'Content-Type';
Const.JSON_TYPE = 'application/json';
Const.URL_ENCODE_TYPE = 'application/x-www-form-urlencoded';
exports.Const = Const;
//# sourceMappingURL=const.component.js.map
