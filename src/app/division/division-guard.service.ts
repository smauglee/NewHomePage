﻿import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, CanDeactivate } from '@angular/router';

import { DivisionEditComponent } from './division-edit.component';

@Injectable()
export class DivisionDetailGuard implements CanActivate {

    constructor(private _router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot): boolean {
        let id = route.url[1].path;
        if (id === "") {
            alert('Invalid division Id');
            // start a new navigation to redirect to list page
            this._router.navigate(['/divisions']);
            // abort current navigation
            return false;
        };
        return true;
    }
}

@Injectable()
export class DivisionEditGuard implements CanDeactivate<DivisionEditComponent> {

    canDeactivate(component: DivisionEditComponent): boolean {
        if (component.divisionForm.dirty) {
            let division_name = component.divisionForm.get('division_name').value || 'New Division';
            return confirm(`Navigate away and lose all changes to ${division_name}?`);
        }
        return true;
    }
}
