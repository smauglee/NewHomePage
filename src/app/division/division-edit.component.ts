﻿import { Component, OnInit, OnDestroy, AfterViewInit, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { ISelectList } from '../shared/selectList';
import { IDivision } from '../models/division';
import { DivisionService } from './division.service';

import { NumberValidator } from '../shared/number.validator';
import { GenericValidator } from '../shared/generic-validator';


@Component({
    templateUrl: './division-edit.component.html'
})
export class DivisionEditComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

    pageTitle: string = 'Division Detail';
    division: IDivision;
    //divisionSelectList: ISelectList[];
    errorMessage: string;
    divisionForm: FormGroup;

    private sub: Subscription;



    // Use with the generic validation message class
    displayMessage: { [key: string]: string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;

    constructor(private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private _divisionService: DivisionService) {

        // Defines all of the validation messages for the form.
        // These could instead be retrieved from a file or database.
        this.validationMessages = {
            division_id: {
                required: 'division id is required.'
            },
            division_name: {
                required: 'division name is required.',
                minlength: 'division name must be at least three characters.',
                maxlength: 'division name cannot exceed 50 characters.'
            },
            sortno: {},
            display: {},
            mode: {}
        };

        // Define an instance of the validator for use with this form, 
        // passing in this form's set of validation messages.
        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngOnInit(): void {
        this.divisionForm = this.fb.group({
            division_id: [{ value: '' }, Validators.required],
            division_name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
            sort_no: [],
            display_flg: [],
            mode: []
        });

        //this.sub = this.route.params.subscribe(
        //    params => {
        //        let id = params['id'];
        //        this.getDivisionSelectList();
        //        this.getDivision(id);
        //    });
    }



    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    ngAfterViewInit(): void {
        // Watch for the blur event from any input element on the form.
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        // Merge the blur event observable with the valueChanges observable
        Observable.merge(this.divisionForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.divisionForm);
        });
    }


    getDivision(id: string) {
        this._divisionService.getDivision(id).subscribe(
            //division => this.division = division,
            (division: IDivision) => this.onDivisiontRetrieved(division),
            error => this.errorMessage = <any>error);
    }

    getDivisionSelectList() {
        this._divisionService.getDivisionList().subscribe(
            //selectList => this.divisionSelectList = selectList,
            error => this.errorMessage = <any>error);
    }

    onDivisiontRetrieved(division: IDivision): void {
        if (this.divisionForm) {
            this.divisionForm.reset();
        }
        this.division = division;

        if (this.division.division_id === null) {
            this.pageTitle = 'Add Division';
        } else {
            this.pageTitle = `Edit Division: ${this.division.division_name}`;
            //this.divisionForm.controls["division_id"].setValue({ value: '', disabled: true }, { onlySelf: true });
            let ctrl = this.divisionForm.get('division_id').disable();
            //ctrl.enabled ? ctrl.disable() : ctrl.enable()
        }

        // Update the data on the form
        this.divisionForm.patchValue({
            division_id: this.division.division_id,
            division_name: this.division.division_name,
            sort_no: this.division.sort_no,
            displya_flg: this.division.display_flg
            //create_user_id: this.division.create_user_id,
            //create_date: this.division.create_date,
            //description: this.division.description
        });

    }

    deleteDivision(): void {
        if (this.division.division_id == "") {
            // Don't delete, it was never saved.
            this.onSaveComplete();
        } else {
            if (confirm(`Really delete the division: ${this.division.division_name}?`)) {
                this._divisionService.deleteDivision(this.division.division_id)
                    .subscribe(
                    () => this.onSaveComplete(),
                    (error: any) => this.errorMessage = <any>error
                    );
            }
        }
    }

    saveDivision(): void {
        if (this.divisionForm.dirty && this.divisionForm.valid) {
            // Copy the form values over the division object values
            let p = Object.assign({}, this.division, this.divisionForm.value);

            this._divisionService.saveDivision(p)
                .subscribe(
                () => this.onSaveComplete(),
                (error: any) => this.errorMessage = <any>error
                );
        } else if (!this.divisionForm.dirty) {
            this.onSaveComplete();
        }
    }

    onSaveComplete(): void {
        // Reset the form to clear the flags
        this.divisionForm.reset();
        this.router.navigate(['/divisions']);
    }

    onBack(): void {
        this.router.navigate(['/divisions']);
    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'Division Detail: ' + message;
    }
}
