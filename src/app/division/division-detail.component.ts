﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

import { IDivision } from '../models/division';
import { DivisionService } from './division.service';

@Component({
    templateUrl: './division-detail.component.html'
})
export class DivisionDetailComponent implements OnInit, OnDestroy {
    pageTitle: string = 'Division Detail';
    division: IDivision;
    errorMessage: string;
    private sub: Subscription;

    constructor(private _route: ActivatedRoute,
        private _router: Router,
        private _divisionService: DivisionService) {
    }

    ngOnInit(): void {
        this.sub = this._route.params.subscribe(
            params => {
                let id = params['id'];
                this.getDivision(id);
            });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    getDivision(id: string) {
        this._divisionService.getDivision(id).subscribe(
            division => this.division = division,
            error => this.errorMessage = <any>error);
    }

    onBack(): void {
        this._router.navigate(['/divisions']);
    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'Division Detail: ' + message;
    }
}
