﻿import { Component, OnInit } from '@angular/core';

import { IDivision } from '../models/division';
import { DivisionService } from './division.service';

@Component({
    templateUrl: './division-list.component.html',
    styleUrls: ['./division-list.component.css']
})
export class DivisionListComponent implements OnInit {
    pageTitle: string = 'Division List';
    imageWidth: number = 50;
    imageMargin: number = 2;
    showImage: boolean = false;
    listFilter: string;
    errorMessage: string;

    divisions: IDivision[];

    constructor(private _divisionService: DivisionService,

    ) {

    }

    toggleImage(): void {
        this.showImage = !this.showImage;
    }

    ngOnInit(): void {

        this._divisionService.getDivisions()
            .subscribe(divisions => this.divisions = divisions,
            error => this.errorMessage = <any>error);

    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'Division List: ' + message;
    }
}
