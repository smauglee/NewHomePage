﻿import { PipeTransform, Pipe } from '@angular/core';
import { IDivision } from '../models/division';

@Pipe({
    name: 'divisionFilter'
})
export class DivisionFilterPipe implements PipeTransform {

    transform(value: IDivision[], filterBy: string): IDivision[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        return filterBy ? value.filter((division: IDivision) =>
            division.division_name.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
    }
}
