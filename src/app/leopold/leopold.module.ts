﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import {
    LeopoldMainComponent,
    //DialogDataExampleDialog
} from './leopold-main.component';
import { LeopoldService } from './leopold.service';

import { SharedModule } from '../shared/shared.module';


@NgModule({
    imports: [
        SharedModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: 'leopold', component: LeopoldMainComponent }
        ])
    ],
    entryComponents: [

    ],
    declarations: [
        LeopoldMainComponent,
        //DialogDataExampleDialog
    ],
    providers: [
        LeopoldService
    ]
})
export class LeopoldModule { }
