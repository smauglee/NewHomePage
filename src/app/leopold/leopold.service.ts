﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';

import { Const } from './../shared/const.component';
import { ISelectList } from './../shared/selectList';
import { IMessage } from './../models/message';

@Injectable()
export class LeopoldService {
    private API_ENDPOINT = Const.BASE_ENDPOINT + 'LeopoldAPI';
    headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON

    constructor(private http: Http) { }
    
    // error
    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    // dataset
    private extractData(response: Response) {
        let body = <IMessage[]>response.json();
        return body || {};
    }

    // post or put
    sendMessage(division: IMessage): Observable<IMessage> {
        let options = new RequestOptions({ headers: this.headers });

        return this.sendMail(division, options);
    }

    // post
    private sendMail(message: IMessage, options: RequestOptions): Observable<IMessage> {
        //division.division_id = undefined;
        return this.http.post(this.API_ENDPOINT, message, options)
            .map(this.extractData)
            .do(data => console.log('sendMessage: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
}
