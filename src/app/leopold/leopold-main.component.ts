﻿import {
    Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, Inject,
    ElementRef, ViewContainerRef, trigger, state, style, transition, animate, keyframes
} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { NumberValidator } from '../shared/number.validator';
import { GenericValidator } from '../shared/generic-validator';
import { IMessage } from '../models/message';

import { LeopoldService } from './leopold.service';
//import { MdDialog,MdDialogRef, MD_DIALOG_DATA } from '@angular/material';

@Component({
    templateUrl: './leopold-main.component.html',
    styleUrls: ['./leopold-main.component.css']
})
export class LeopoldMainComponent implements OnInit {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];
    messageForm: FormGroup;
    private sub: Subscription;
    pageTitle: string = 'Leopold';
    imageWidth: number = 50;
    imageMargin: number = 2;
    errorMessage: string;
    loading = false;
    message: IMessage;
    sendButtonText: string = "お問い合わせ";
    sendComplete: string = "";

    displayMessage: { [key: string]:   string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;
    
    constructor(
        //private dialog: MdDialog,
        private leopoldService: LeopoldService,
        private fb: FormBuilder) {
        this.validationMessages = {
            username: {
                required: '必須項目です。',
                maxlength: '20文字以内に入力して下さい。'
            },
            company: {
                required: '必須項目です。',
                maxlength: '20文字以内に入力して下さい。'
            },
            tel: {
                required: '必須項目です。',
                maxlength: '14文字以内に入力して下さい。'
            },
            email: {
                required: '必須項目です。',
                maxlength: '50文字以内に入力して下さい。'
            },
            homepage: {
                required: '必須項目です。',
                maxlength: '50文字以内に入力して下さい。'
            },
            message: {
                required: '必須項目です。'
            }
        };

        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    
    ngOnInit(): void {
        this.messageForm = this.fb.group({
            username: ['', [Validators.required, Validators.maxLength(20)]],
            company: ['', [Validators.required, Validators.maxLength(20)]],
            tel: ['', [Validators.required, Validators.maxLength(14)]],
            email: ['', [Validators.required, Validators.maxLength(50)]],
            homepage: ['', [Validators.required, Validators.maxLength(20)]],
            message: ['', Validators.required]
        });

        
    }

    ngOnDestroy(): void {
        //this.sub.unsubscribe();
    }

    ngAfterViewInit(): void {
        // Watch for the blur event from any input element on the form.
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        // Merge the blur event observable with the valueChanges observable
        Observable.merge(this.messageForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.messageForm);
        });
    }

    sendProcess() {
        this.loading = true;
        let c = Object.assign({}, this.message, this.messageForm.value);
        this.leopoldService.sendMessage(this.messageForm.value)
            .subscribe(result => {
                // message display
                //this.messageForm.
                // initialization
                //this.ngOnInit();
                this.loading = false;
                
            });
this.sendComplete = "OK";    
                this.messageForm.reset();
        //this.dialog.open(DialogDataExampleDialog, {});
    }
}
//@Component({
//    selector: 'dialog-data-example-dialog',
//    template: 'Favorite Animal',
//})
//export class DialogDataExampleDialog {
//    constructor( @Inject(MD_DIALOG_DATA) public data: any) { }
//}