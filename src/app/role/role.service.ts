﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';

import { Const } from './../shared/const.component';
import { IRole } from '../models/role';
import { ISelectList } from './../shared/selectList';

@Injectable()
export class RoleService {
    private API_ENDPOINT = Const.BASE_ENDPOINT + 'MstRole';
    headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON

    constructor(private http: Http) { }

    initializeRole(): IRole {
        // Return an initialized object
        return {
            role_id: null,
            role_name: null,
            create_date: null,
            create_user_id: null,
            update_date: null,
            update_user_id: null
        };
    }

    // list
    getRoles(): Observable<IRole[]> {
        console.log(this.API_ENDPOINT);
        return this.http.get(this.API_ENDPOINT)
            .map(this.extractData)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // detail or edit
    getRole(id: string): Observable<IRole> {
        if (id == "0") {
            return Observable.create((observer: any) => {
                observer.next(this.initializeRole());
                observer.complete();
            });
        }
        return this.http.get(`${this.API_ENDPOINT}/${id}`)
            .map(this.extractData)
            .do(data => console.log('role: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // delete
    deleteRole(id: string): Observable<Response> {
        let options = new RequestOptions({ headers: this.headers });

        const url = `${this.API_ENDPOINT}/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteRole: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // post or put
    saveRole(role: IRole): Observable<IRole> {
        let options = new RequestOptions({ headers: this.headers });

        if (role.create_date == null) {
            return this.createRole(role, options);
        }
        return this.updateRole(role, options);
    }

    // post
    private createRole(role: IRole, options: RequestOptions): Observable<IRole> {
        //role.role_id = undefined;
        return this.http.post(this.API_ENDPOINT, role, options)
            .map(this.extractData)
            .do(data => console.log('createRole: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // put
    private updateRole(role: IRole, options: RequestOptions): Observable<IRole> {
        const url = `${this.API_ENDPOINT}/${role.role_id}`;
        //const url = `${this.API_ENDPOINT}`;
        return this.http.put(url, role, options)
            .map(() => role)
            .do(data => console.log('updateRole: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // role_select_list
    getRoleList(): Observable<ISelectList[]> {
        console.log(Const.BASE_ENDPOINT + 'MstRole');
        return this.http.get(Const.BASE_ENDPOINT + 'MstRole')
            .map(this.extractData)
            .do(data => console.log('All Role: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // dataset
    private extractData(response: Response) {
        let body = <IRole[]>response.json();
        return body || {};
    }

    // error
    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
