﻿import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, CanDeactivate } from '@angular/router';

import { RoleEditComponent } from './role-edit.component';

@Injectable()
export class RoleDetailGuard implements CanActivate {

    constructor(private _router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot): boolean {
        let id = route.url[1].path;
        if (id === "") {
            alert('Invalid role Id');
            // start a new navigation to redirect to list page
            this._router.navigate(['/roles']);
            // abort current navigation
            return false;
        };
        return true;
    }
}

@Injectable()
export class RoleEditGuard implements CanDeactivate<RoleEditComponent> {

    canDeactivate(component: RoleEditComponent): boolean {
        if (component.roleForm.dirty) {
            let role_name = component.roleForm.get('role_name').value || 'New Role';
            return confirm(`Navigate away and lose all changes to ${role_name}?`);
        }
        return true;
    }
}
