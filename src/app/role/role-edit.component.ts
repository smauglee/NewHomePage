﻿import { Component, OnInit, OnDestroy, AfterViewInit, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { ISelectList } from '../shared/selectList';
import { IRole } from '../models/role';
import { RoleService } from './role.service';

import { NumberValidator } from '../shared/number.validator';
import { GenericValidator } from '../shared/generic-validator';


@Component({
    templateUrl: './role-edit.component.html'
})
export class RoleEditComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

    pageTitle: string = 'Role Detail';
    role: IRole;
    roleSelectList: ISelectList[];
    errorMessage: string;
    roleForm: FormGroup;

    private sub: Subscription;



    // Use with the generic validation message class
    displayMessage: { [key: string]: string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;

    constructor(private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private _roleService: RoleService) {

        // Defines all of the validation messages for the form.
        // These could instead be retrieved from a file or database.
        this.validationMessages = {
            role_id: {
                required: 'role id is required.'
            },
            role_name: {
                required: 'role name is required.',
                minlength: 'role name must be at least three characters.',
                maxlength: 'role name cannot exceed 50 characters.'
            }
        };

        // Define an instance of the validator for use with this form, 
        // passing in this form's set of validation messages.
        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngOnInit(): void {
        this.roleForm = this.fb.group({
            role_id: [{ value: '' }, Validators.required],
            role_name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
            create_role_id: [{ value: '' }],
            create_date: [{ value: '' }],
            update_role_id: [{ value: '' }],
            update_date: [{ value: '' }]
        });

        this.sub = this.route.params.subscribe(
            params => {
                let id = params['id'];
                this.getRoleSelectList();
                this.getRole(id);
            });
    }



    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    ngAfterViewInit(): void {
        // Watch for the blur event from any input element on the form.
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        // Merge the blur event observable with the valueChanges observable
        Observable.merge(this.roleForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.roleForm);
        });
    }


    getRole(id: string) {
        this._roleService.getRole(id).subscribe(
            //role => this.role = role,
            (role: IRole) => this.onRoletRetrieved(role),
            error => this.errorMessage = <any>error);
    }

    getRoleSelectList() {
        this._roleService.getRoleList().subscribe(
            selectList => this.roleSelectList = selectList,
            error => this.errorMessage = <any>error);
    }

    onRoletRetrieved(role: IRole): void {
        if (this.roleForm) {
            this.roleForm.reset();
        }
        this.role = role;

        if (this.role.role_id === null) {
            this.pageTitle = 'Add Role';
        } else {
            this.pageTitle = `Edit Role: ${this.role.role_name}`;
            //this.roleForm.controls["role_id"].setValue({ value: '', disabled: true }, { onlySelf: true });
            let ctrl = this.roleForm.get('role_id').disable();
            //ctrl.enabled ? ctrl.disable() : ctrl.enable()
        }

        // Update the data on the form
        this.roleForm.patchValue({
            role_id: this.role.role_id,
            role_name: this.role.role_name,
            create_user_id: this.role.create_user_id,
            create_date: this.role.create_date,
            //description: this.role.description
        });

    }

    deleteRole(): void {
        if (this.role.role_id == "") {
            // Don't delete, it was never saved.
            this.onSaveComplete();
        } else {
            if (confirm(`Really delete the role: ${this.role.role_name}?`)) {
                this._roleService.deleteRole(this.role.role_id)
                    .subscribe(
                    () => this.onSaveComplete(),
                    (error: any) => this.errorMessage = <any>error
                    );
            }
        }
    }

    saveRole(): void {
        if (this.roleForm.dirty && this.roleForm.valid) {
            // Copy the form values over the role object values
            let p = Object.assign({}, this.role, this.roleForm.value);

            this._roleService.saveRole(p)
                .subscribe(
                () => this.onSaveComplete(),
                (error: any) => this.errorMessage = <any>error
                );
        } else if (!this.roleForm.dirty) {
            this.onSaveComplete();
        }
    }

    onSaveComplete(): void {
        // Reset the form to clear the flags
        this.roleForm.reset();
        this.router.navigate(['/roles']);
    }

    onBack(): void {
        this.router.navigate(['/roles']);
    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'Role Detail: ' + message;
    }
}
