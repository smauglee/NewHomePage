﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

import { IRole } from '../models/role';
import { RoleService } from './role.service';

@Component({
    templateUrl: './role-detail.component.html'
})
export class RoleDetailComponent implements OnInit, OnDestroy {
    pageTitle: string = 'Role Detail';
    role: IRole;
    errorMessage: string;
    private sub: Subscription;

    constructor(private _route: ActivatedRoute,
        private _router: Router,
        private _roleService: RoleService) {
    }

    ngOnInit(): void {
        this.sub = this._route.params.subscribe(
            params => {
                let id = params['id'];
                this.getRole(id);
            });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    getRole(id: string) {
        this._roleService.getRole(id).subscribe(
            role => this.role = role,
            error => this.errorMessage = <any>error);
    }

    onBack(): void {
        this._router.navigate(['/roles']);
    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'Role Detail: ' + message;
    }
}
