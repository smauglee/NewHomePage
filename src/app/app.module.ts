﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { MdDialogModule } from '@angular/material';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './home/welcome.component';
import { LeopoldMainComponent } from './leopold/leopold-main.component';

import { SharedModule } from './shared/shared.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { DivisionModule } from './division/division.module';
import { RoleModule } from './role/role.module';

import { LeopoldModule } from './leopold/leopold.module';
@NgModule({
  declarations: [
      AppComponent,
      WelcomeComponent,
  ],
  imports: [
      BrowserModule,
      BrowserAnimationsModule,
      RouterModule.forRoot([
          { path: 'leopold', component: LeopoldMainComponent },
          { path: 'welcome', component: WelcomeComponent },
          { path: '', redirectTo: 'leopold', pathMatch: 'full' },
          { path: '**', redirectTo: 'leopold', pathMatch: 'full' },
      ], { useHash: true }),
    FormsModule,
    HttpModule,
      
      //MdDialogModule,
      SharedModule,
      AuthModule,
      UserModule,
      DivisionModule,
      RoleModule,
      LeopoldModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
