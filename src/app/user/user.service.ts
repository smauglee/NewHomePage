﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';

import { Const } from './../shared/const.component';
import { IUser } from '../models/user';
import { ISelectList } from './../shared/selectList';

@Injectable()
export class UserService {
    private API_ENDPOINT = Const.BASE_ENDPOINT + 'MstUser';
    headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON

    constructor(private http: Http) { }

    initializeUser(): IUser {
        // Return an initialized object
        return {
            user_id: null,
            user_name: null,
            email: null,
            password: null,
            role_id: null,
            role_name: null,
            division_id: null,
            division_name: null,
            tel_no: null,
            birthday: null,
            post_no: null,
            city_id: null,
            city_name: null,
            address_1: null,
            address_2: null,
            sort_no: null,
            bank_id: null,
            bank_name: null,
            bank_account_type: null,
            bank_account_no: null,
            bank_branch_no: null,
            bank_branch_name: null,
            create_date: null,
            create_user_id: null,
            update_date: null,
            update_user_id: null
        };
    }

    // list
    getUsers(): Observable<IUser[]> {
        console.log(this.API_ENDPOINT);
        return this.http.get(this.API_ENDPOINT)
            .map(this.extractData)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // detail or edit
    getUser(id: string): Observable<IUser> {
        if (id == "0") {
            return Observable.create((observer: any) => {
                observer.next(this.initializeUser());
                observer.complete();
            });
        }
        return this.http.get(`${this.API_ENDPOINT}/${id}`)
            .map(this.extractData)
            .do(data => console.log('user: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // delete
    deleteUser(id: string): Observable<Response> {
        let options = new RequestOptions({ headers: this.headers });

        const url = `${this.API_ENDPOINT}/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteUser: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // post or put
    saveUser(user: IUser): Observable<IUser> {
        let options = new RequestOptions({ headers: this.headers });

        if (user.create_date == null) {
            return this.createUser(user, options);
        }
        return this.updateUser(user, options);
    }

    // post
    private createUser(user: IUser, options: RequestOptions): Observable<IUser> {
        //user.user_id = undefined;
        return this.http.post(this.API_ENDPOINT, user, options)
            .map(this.extractData)
            .do(data => console.log('createUser: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // put
    private updateUser(user: IUser, options: RequestOptions): Observable<IUser> {
        const url = `${this.API_ENDPOINT}/${user.user_id}`;
        //const url = `${this.API_ENDPOINT}`;
        return this.http.put(url, user, options)
            .map(() => user)
            .do(data => console.log('updateUser: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // division_select_list
    getDivisionList(): Observable<ISelectList[]> {
        console.log(Const.BASE_ENDPOINT + 'MstDivision');
        return this.http.get(Const.BASE_ENDPOINT + 'MstDivision')
            .map(this.extractData)
            .do(data => console.log('All Division: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // role_select_list
    getRoleList(): Observable<ISelectList[]> {
        console.log(Const.BASE_ENDPOINT + 'MstRole');
        return this.http.get(Const.BASE_ENDPOINT + 'MstRole')
            .map(this.extractData)
            .do(data => console.log('All Role: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // dataset
    private extractData(response: Response) {
        let body = <IUser[]>response.json();
        return body || {};
    }

    // error
    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
