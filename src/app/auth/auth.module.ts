﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';

import { SharedModule } from '../shared/shared.module';

import { LoginComponent } from './login.component';

@NgModule({
    imports: [
        SharedModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: 'login', component: LoginComponent},
           // { path: 'user/:id', component: UserDetailComponent, canActivate: [UserDetailGuard, AuthGuard] },
           // { path: 'userEdit/:id', component: UserEditComponent, canDeactivate: [UserEditGuard, AuthGuard] }
        ])
    ],
    entryComponents: [

    ],
    declarations: [
        LoginComponent,
    ],
    providers: [
        AuthGuard,
        AuthService
    ]
})
export class AuthModule { }
