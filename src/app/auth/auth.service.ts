﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import { ILogin } from './../models/login';
import { IRegister } from './../models/register';
import { Const } from './../shared/const.component';
@Injectable()
export class AuthService {
    public token: string;
    //login: ILogin;
    private API_ENDPOINT = Const.BASE_ENDPOINT + 'Token';
    headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    headersToken = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); // ... Set content type to JSON
    
    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }
    //11_29min
    loginProcess(login: ILogin): Observable<boolean> {
        let options = new RequestOptions({ headers: this.headersToken });
        let trans_data;
        for (var key in login) {
            if (trans_data) {
                trans_data += '&' + key + '=' + login[key];
            } else {
                trans_data = key + '=' + login[key];
            }
        }
        return this.http.post("http://samugserver.azurewebsites.net/token", trans_data, options)
            //return this.http.post(API_ENDPOINT: string, JSON.stringify({ login }))
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().access_token;
                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ login, token: token }));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    registerProcess(register: IRegister): Observable<boolean> {
        let options = new RequestOptions({ headers: this.headers });
        return this.http.post(Const.BASE_ENDPOINT + "Account/Register", register, options)
            //return this.http.post(API_ENDPOINT: string, JSON.stringify({ login }))
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('register', JSON.stringify({ register, token: token }));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}