﻿export interface IRole {
    role_id: string;
    role_name: string;
    create_date: string;
    create_user_id: string;
    update_date: string;
    update_user_id: string;
}