﻿export interface IMessage {
    username: string;
    company: string;
    tel: string;
    email: string;
    homepage: string;
    message: string;
}