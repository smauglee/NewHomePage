﻿export interface IDivision {
    division_id: string;
    division_name: string;
    sort_no: number;
    display_flg: string;
    mode: string;
}