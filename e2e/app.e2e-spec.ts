import { TokenAniPage } from './app.po';

describe('token-ani App', () => {
  let page: TokenAniPage;

  beforeEach(() => {
    page = new TokenAniPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
